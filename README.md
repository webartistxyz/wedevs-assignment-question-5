# weDevs_Assignment Question 5

## Clone the repository

```
git clone https://gitlab.com/webartistxyz/wedevs-assignment-question-5
```

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
